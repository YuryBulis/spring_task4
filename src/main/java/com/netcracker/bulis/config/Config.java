package com.netcracker.bulis.config;

import com.netcracker.bulis.hibernate.dao.CategoryDao;
import com.netcracker.bulis.hibernate.dao.OfferDao;
import com.netcracker.bulis.hibernate.dao.impl.CategoryDaoImpl;
import com.netcracker.bulis.hibernate.dao.impl.OfferDaoImpl;
import com.netcracker.bulis.services.CategoryService;
import com.netcracker.bulis.services.OfferService;
import com.netcracker.bulis.services.SimpleCategoryService;
import com.netcracker.bulis.services.SimpleOfferService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.bulis")
public class Config {

    @Bean
    public CategoryDao getCategoryDaoImpl(){
        return new CategoryDaoImpl();
    }

    @Bean
    public OfferDao getOfferDaoImpl(){
        return new OfferDaoImpl();
    }

    @Bean
    public OfferService offerService (OfferDao offerDao){
        return new SimpleOfferService(offerDao);
    }

    @Bean
    public CategoryService categoryService (CategoryDao categoryDao){
        return new SimpleCategoryService(categoryDao);
    }

}
