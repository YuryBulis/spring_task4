package com.netcracker.bulis.config;

import com.netcracker.bulis.entities.Category;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.bulis")
public class CategoryConfig {

    @Bean
    public Category category(){
        return new Category();
    }
}
