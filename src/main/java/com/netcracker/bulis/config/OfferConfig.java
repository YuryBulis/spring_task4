package com.netcracker.bulis.config;

import com.netcracker.bulis.entities.Offer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.bulis")
public class OfferConfig {

    @Bean
    public Offer offer(){
        return new Offer();
    }
}
