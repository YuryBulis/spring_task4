package com.netcracker.bulis.services;

import com.netcracker.bulis.entities.Offer;

import java.util.UUID;

public interface OfferService {

    Offer createOffer(Offer offer);

    Offer findOfferById(UUID id);

    Offer updateOffer(Offer offer);

    void deleteOfferById(UUID id);
}
