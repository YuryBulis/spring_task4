package com.netcracker.bulis.services;

import com.netcracker.bulis.entities.Category;
import com.netcracker.bulis.hibernate.dao.CategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimpleCategoryService implements CategoryService {

    private final CategoryDao categoryDao;

    @Autowired
    public SimpleCategoryService(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public Category createCategory(Category category) {
        return categoryDao.createCategory(category);
    }

    @Override
    public Category findCategoryByName(String name) {
        return categoryDao.findCategoryByName(name);
    }

    @Override
    public Category updateCategory(Category category) {
        return categoryDao.updateCategory(category);
    }

    @Override
    public void deleteCategory(Category category) {
        categoryDao.deleteCategory(category);
    }
}
