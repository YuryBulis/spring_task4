package com.netcracker.bulis.services;

import com.netcracker.bulis.entities.Category;

public interface CategoryService {

    Category createCategory(Category category);

    Category findCategoryByName(String name);

    Category updateCategory(Category category);

    void deleteCategory(Category category);
}
