package com.netcracker.bulis.services;

import com.netcracker.bulis.entities.Offer;
import com.netcracker.bulis.hibernate.dao.OfferDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SimpleOfferService implements OfferService {
    private final OfferDao offerDao;

    @Autowired
    public SimpleOfferService(OfferDao offerDao) {
        this.offerDao = offerDao;
    }

    @Override
    public Offer createOffer(Offer offer) {
        return offerDao.createOffer(offer);
    }

    @Override
    public Offer findOfferById(UUID id) {
        return offerDao.findOfferById(id);
    }

    @Override
    public Offer updateOffer(Offer offer) {
        return  offerDao.updateOffer(offer);
    }

    @Override
    public void deleteOfferById(UUID id) {
        offerDao.deleteOfferById(id);
    }
}
