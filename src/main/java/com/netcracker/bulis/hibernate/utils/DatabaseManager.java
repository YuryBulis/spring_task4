package com.netcracker.bulis.hibernate.utils;

import javax.persistence.EntityManager;

public interface DatabaseManager {
    EntityManager getEntityManager();
}