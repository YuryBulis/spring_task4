package com.netcracker.bulis.hibernate.dao.impl;

import com.netcracker.bulis.entities.Category;
import com.netcracker.bulis.entities.Offer;
import com.netcracker.bulis.hibernate.dao.CategoryDao;
import com.netcracker.bulis.hibernate.dao.OfferDao;
import com.netcracker.bulis.hibernate.utils.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.UUID;

public class OfferDaoImpl implements OfferDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();
    CategoryDao categoryDao = new CategoryDaoImpl();

    public OfferDaoImpl() {
    }

    @Override
    public Offer createOffer(Offer offer) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(offer);
        tx.commit();
        return offer;
    }

    @Override
    public List<Offer> getAllOffers() {
        EntityTransaction tx = entityManager.getTransaction();
        List<Offer> offers;
        tx.begin();
        offers = entityManager.createQuery("FROM Offer offer", Offer.class).getResultList();
        return offers;

    }

    @Override
    public List<Offer> getOffersWithSameCategory() {
        EntityTransaction tx = entityManager.getTransaction();
        List<Offer> offers;
        tx.begin();
        offers = entityManager.createQuery("FROM Offer offer WHERE category.categoryName = 'Pizza'", Offer.class).getResultList();
        return offers;
    }

    @Override
    public Offer findOfferById(UUID id) {
        EntityTransaction tx = entityManager.getTransaction();
        Offer foundOffer;
        tx.begin();
        foundOffer = entityManager.find(Offer.class, id);
        tx.commit();
        return foundOffer;
    }

    @Override
    public void deleteOfferById(UUID id) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        Offer offer = entityManager.getReference(Offer.class, id);
        entityManager.remove(offer);
        tx.commit();
    }

    @Override
    public Offer updateOffer(Offer offer) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(offer);
        tx.commit();
        return offer;
    }

    @Override
    public Offer changeCategory(Offer offer, Category category, Category changedCategory) {
        categoryDao.deleteCategory(category);
        offer.setCategory(changedCategory);
        updateOffer(offer);
        return offer;
    }

}
