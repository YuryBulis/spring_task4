package com.netcracker.bulis.hibernate.dao;

import com.netcracker.bulis.entities.Category;

import java.util.HashSet;
import java.util.Set;

public interface CategoryDao {
    Category createCategory(Category category);

    Category findCategoryByName(String name);

    Category updateCategory(Category category);

    void deleteCategory(Category category);

    Set<Category> createSeveralCategories(HashSet<Category> categories);
}
