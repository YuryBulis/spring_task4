package com.netcracker.bulis.hibernate.dao.impl;

import com.netcracker.bulis.entities.Category;
import com.netcracker.bulis.hibernate.dao.CategoryDao;
import com.netcracker.bulis.hibernate.utils.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.HashSet;
import java.util.Set;

public class CategoryDaoImpl implements CategoryDao {
    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    public CategoryDaoImpl() {
    }

    @Override
    public Category createCategory(Category category) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(category);
        tx.commit();
        return category;
    }

    @Override
    public Category findCategoryByName(String name) {
        EntityTransaction tx = entityManager.getTransaction();
        Category foundCategory;
        tx.begin();
        foundCategory = entityManager
                .createQuery("FROM Category category WHERE category.categoryName = :name", Category.class)
                .setParameter("name", name).getSingleResult();
        tx.commit();
        return foundCategory;
    }

    @Override
    public Category updateCategory(Category category) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(category);
        tx.commit();
        return category;
    }

    @Override
    public void deleteCategory(Category category) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.remove(category);
        tx.commit();
    }

    @Override
    public Set<Category> createSeveralCategories(HashSet<Category> categories) {
        for (Category category : categories) {
            createCategory(category);
        }
        return categories;
    }
}
