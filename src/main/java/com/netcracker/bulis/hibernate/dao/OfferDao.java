package com.netcracker.bulis.hibernate.dao;

import com.netcracker.bulis.entities.Category;
import com.netcracker.bulis.entities.Offer;

import java.util.List;
import java.util.UUID;

public interface OfferDao {
    Offer createOffer(Offer offer);

    List<Offer> getAllOffers();

    List<Offer> getOffersWithSameCategory();

    Offer findOfferById(UUID id);

    void deleteOfferById(UUID id);

    Offer updateOffer(Offer offer);

    Offer changeCategory(Offer offer, Category category, Category changedCategory);




}
