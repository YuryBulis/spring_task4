package com.netcracker.bulis;

import com.netcracker.bulis.config.CategoryConfig;
import com.netcracker.bulis.config.Config;
import com.netcracker.bulis.config.OfferConfig;
import com.netcracker.bulis.entities.Category;
import com.netcracker.bulis.entities.Offer;
import com.netcracker.bulis.services.OfferService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class)
public class TestSimpleCategoryService {

    @Autowired
    private OfferService offerService;

    private ApplicationContext offerContext;

    private ApplicationContext categoryContext;

    private Offer testOffer;

    private Category testCategory;

    @BeforeEach
    public void setUp(){
        offerContext = new AnnotationConfigApplicationContext(OfferConfig.class);
        categoryContext = new AnnotationConfigApplicationContext(CategoryConfig.class);

        testOffer = offerContext.getBean("offer", Offer.class);
        testCategory = categoryContext.getBean("category", Category.class);

        testCategory.setCategoryName("books");
        testOffer.setName("Harry Potter");
        testOffer.setCategory(testCategory);
    }

    @Test
    public void testCreateOrder(){
        Offer createdOffer = offerService.createOffer(testOffer);
        assertNotNull(createdOffer);
    }

    @Test
    public void testFindOfferById() {
        Offer createdOffer = offerService.createOffer(testOffer);
        Offer foundedOffer = offerService.findOfferById(createdOffer.getId());
        assertNotNull(foundedOffer);
    }

    @Test
    public void testUpdateOffer(){
        Offer createdOffer = offerService.createOffer(testOffer);
        createdOffer.setName("Sherlock");
        Offer updatedOffer = offerService.updateOffer(createdOffer);

        assertEquals(updatedOffer.getId(), createdOffer.getId());
    }

    @Test
    public void testDeleteOfferById(){
        Offer createdOffer = offerService.createOffer(testOffer);
        offerService.deleteOfferById(createdOffer.getId());

        assertNull(offerService.findOfferById(createdOffer.getId()));
    }
}
